\section{Introduction}\label{intro}

\subsection{Motivation}

Reviewing patient’s biological tissue samples by a pathologist is a conventional method for many diseases diagnosis, especially for cancer such as breast cancer. However, reviewing samples are laborious and time-intensive, which may delay decision-making. The reviewing of pathology slides is a very complex task. Sometimes agreement in diagnosis for some forms of breast cancer can be as low as 48\% \cite{past1}. The difficulty in diseases diagnosis by pathologists is inevitable because the pathologists need to review all slides per patient while each of slide is 10+ gigapixels when digitized at 40X magnification.

On the other hand, current automatic medical diagnosis attempts are not targeted at pathologists with little artificial intelligence background. Pathologist may not understand terms describing an AI or statistics an AI produces. There exists possibility that pathologist cannot interpret a computer generated report very well. With such limitation, cooperating with AI may instead delay decision-making. Therefore, we will try to implement a complete automated breast cancer diagnosis system.

\subsection{Background}

Since AlphaGo showed the possibility that AI can beat human in real world tasks \cite{past2}, more and more people in universities or industries are interested in AI for medical usage. The number of papers about AI diagnosis is growing exponentially.

\subsubsection{Development of AI Classifier}

The classification problem is an important component in the field of deep learning. It is targeted on judging a new sample belongs to which predefined sample category, according to a train set containing certain number of known samples. The classification problem is also called supervised classification, since all samples in train set are labeled, and all categories are predefined \cite{past6}. Classifier is one of the pattern recognition applications.

The most widely applied AI classifier is spam email filter, which classify each email into “regular” or “junk”. Generally speaking, each instance in the classification problem will be transform into a computer analyzable vector, which is usually called “features”. A feature can be an enumeration or a number.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-classifier}
	\caption{AI classification}
\end{figure}

Then the Naïve Bayes classifier was proposed in 1950s. It is a group of simple classifiers derived from the Bayes’ Theorem, assuming that all features in the samples are strongly independent. Since its publish, it has been widely researched. Things turned out that it performed well for text classification, with number of occurrence of words as features. It can do the aforementioned email classification task at a relatively low computation amount compared to more recent algorithms while still achieve acceptable accuracy \cite{past7}. With appropriate preprocessing, it is still competitive.
$$ \mbox{posterior} = \frac{\mbox{prior} \times \mbox{liklihood}}{\mbox{evidence}} $$
$$ \Pr(C_k|F_1,\ldots,F_n) = \frac{1}{Z} \Pr(C_k) \sum_{i=1}^n \Pr (F_i|C_k) $$
$$ \mbox{classify}(f_1,\ldots,f_n) = \arg \max \Pr(C=c) \prod_{i=1}^n \Pr (F_i=f_i|C=c) $$

The Naïve Bayes classifier can have different assumptions for the underlying distribution of features. For continuous variables, we can assume they are under the classic Gaussian distribution. For text data, the standard assumption is multinomial distribution, where the number of occurrence of a word is taken into account. A simplified version is Bernoulli distribution, which only consider whether a word appears or not. 

The Naïve Bayes classifier is much more extensible than other algorithms. Number of parameters it needs to learn is linear to number of features, therefore the training time complexity is also linear. Moreover, the training process has a well close-formed expression. For email classification problem, the number of parameters is merely the number of unique words in all emails. This avoid the expensive linear approximation many other classifiers use.

Later Support Vector Machine (SVM) was introduced by Vladimir Naumovich Vapnik and Alexey Yakovlevich Chervonenkis \cite{past8}. Given a train set, each sample is represented by a point the hyperspace. For SVM, samples are treated as p-dimensional vectors; SVM assumes that we can separate these points with a (p-1)-dimensional hyper plain. There may be may such hyper plain, and SVM will separate different categories with a hyper plain with as large margin as possible. Thus, we will get the hyper plain whose distance to nearest data points of two categories is maximized. This is also why it was named “Support Vector” machine.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-svm}
	\caption{Support Vector Machine}
\end{figure}

SVM is usually a linear classifier. However, with some tricks called “Kernel trick”, SVM can also do nonlinear classification. The main idea is, by mapping the original sample space to a higher dimensional space, the original non-linear separable set may become separable.

\subsubsection{Development of AI Object Detection}

As the demand is rising for autonomous vehicles, intelligent monitoring and various other applications, object detection is becoming a hot spot in the AI field. These system need to not only recognize and classify the whole images, but also locating each objects in images. We want to find faces or cars from a complex real-world image. This makes object detection a hard task compared to the traditional classification.

The very first object detection algorithm was based on extracting features from the images \cite{matlabfeaturematching}. The core idea of this algorithm is simple. By finding correspondence between the given reference and the input target object, we can detect a specific object. It estimate the scale transformation and the rotation applied, and find the matching features. It can also handle some level of occlusion.

Feature-based object detection requires the object has a unique texture pattern, which reduces noise in feature matches. The performance is unlikely to be good for large objects with a uniform color, such as a car. Plus, it is designed to find a specific instance rather than a class of instances, i.e. to find the one car rather than any cars. Unfortunately, these limitations restrict its usage.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-feature-object-detection}
	\caption{Object Detection by Point Feature Matching}
\end{figure}

Viola-Jones object detection was later proposed in 2001 by Paul Viola and Michael Jones \cite{viola2001rapid}. It is the first algorithm that can process the image in real-time and still gives satisfiable detection rate. While it is capable for multiple class object detection, it was originally designed and mainly used for face detection. It uses features extracted from sums of rectangular areas in the image, which is called harr features. As for the face detection task, this algorithm find the similar features in all faces, and then represents the image with an integral image. The summing process can be done in constant time, so this algorithm is faster than its competitors. It then uses a cascade architecture to assemble a strong classifier from many weak classifiers. This architecture tolerates poor weak classifiers very well, which also reduces its running time.

Viola-Jones algorithm was the first one that enables object detection in the consumer technologies. Face detection functions on nowadays cameras are mainly based on this algorithm. It however is still a feature based solution, so still has the same drawback as other similar solutions. Its performance drops when detecting multiple classes.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-harr-object-detection}
	\caption{Viola-Jones Object Detection}
\end{figure}

Then SVM classification was also introduced to this area by Navneet Dalal and Bill Triggs to further improve performance \cite{dalal2005histograms}. With histograms of oriented gradients (HOG) features, we can achieve a higher accuracy. This method extract HOG descriptors from positive and negative samples, then train a linear SVM on these descriptors. It then apply hard-negative mining on inputs, i.e. slide a window through the image and compute the SVM at each window. Since SVM can give an accuracy boost in classification task, it is not surprising that it will also give one in object detection task.

\subsubsection{Development of Deep Learning}

Deep learning is a subset of machine learning. It is a family of feature learning algorithms in the area of machine learning. Observation values can be represented in various ways, such as a vector containing RGB values of each pixel, or more abstractly a series of edges and areas \cite{past9}. It attempts to do highly abstract data computation with multiple process layers which may contain a complicated structure or non-linear mapping. In general, it is a boarder machine learning method, as it is not specific to any task. There are multiple deep learning frameworks already widely used, such as deep neural network, convolutional neural network and recursive neural network. Deep learning has been widely used in applications, including computer vision, natural language processing and bioinformatics, and achieves supreme results.

In 1989, Yann LeCun \cite{past10} proposed the deep learning mode. Through it could run, the computation cost was so large that the training took about three days. The very first deep learning attempt therefore failed going into real application. The trend of AI then shifted into Support Vector Machine. However, in 1992, Schmidhuber \cite{past11} proposed an effective algorithm to train neural networks. This algorithm treats each layer in the network as an unsupervised, and then tune its parameters with supervised back propagation algorithm. In experiment, it was shown that this training method can indeed improve the train speed of supervised learning.

The advantage of deep learning is that it uses effective unsupervised or Semi-supervised feature learning and layered feature extraction instead of man-powered feature extraction. The aim of feature learning is to seek for better representation of data and to create better model to learn these representations from large-scale unlabeled dataset. The representation is like development of real neural network, and is based on the understanding of how information is processed and transmitted in neural-like systems \cite{past12}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-dnn}
	\caption{A deep neural network}
	\label{intro:dnn}
\end{figure}

The basis of deep learning is the distributed representation in machine learning. “Distributed” means the assumption that the observation is resulted from interaction between different factors. Furthermore, deep learning assumes that such interaction can be spliced into multiple layers, which means the multiple abstraction of the observed value. Different number of layers and different size of layers can be used to represent different degree of abstraction. This idea of layered abstraction indicates that higher-level concepts are learned from lower-level concepts. This structure is usually constructed with greedy algorithm, which helps the machine to learn more significant features. Many deep learning methods are unsupervised algorithms, which enables deep learning to be applied to unlabeled data. This is a great advantage over other algorithms. The amount of available unlabeled data is much larger than labeled ones; unlabeled data is also cheaper to acquire.

What even more encouraged researchers is General-Purpose computing on Graphics Processing Units (GPGPU). The development of more powerful hardware and increase in available data made deeper neural networks realizable. In 2009, Nvidia stepped into the area of deep learning and started promoting its GPU. It was confirmed that the involvement of GPU can increase the training speed by more than 100 times. Since GPU is quite suitable for matrix/vector computation in deep learning algorithm, a GPU can reduce the time required from weeks to days.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-cpu-to-gpu}
	\caption{From CPU to GPU}
\end{figure}

Since the emerge of deep learning, it has become one part of the most advanced systems in various areas, especially in computer vision and speed recognition. On standard verification datasets such as Cifar 10, experiments showed that deep learning can improve recognition accuracy. A deep learning method, convolution neural network, processed about 10\% to 20\% checks in US. Due to the development of deep learning, the year 2010 witnessed a bunch of the very first industrial speech recognition products.

\subsubsection{Development of Deep Learning for Medical Images}

In the area of medical image proceeding, deep learning is becoming more and more attractive. The recent development in deep learning has achieved a great leap. Generally speaking, research on deep learning for medical images is mainly focused on four aspects: structures detection, segmentation, labeling and captioning, and computer aided detection or diagnosis.

Structure detection is one of the most important steps in medical image process. Pathologists generally accomplish this task by recognizing some anatomical feature in the image. Though the success of deep learning in this area mainly depends on how many anatomical feature the algorithm can extract. The recent trend indicates deep learning is mature enough to solve real world problems. Shin et al. \cite{past15} proved deep learning in computer vision applicable for medical images. On top of this, they detected multiple organs in a series of MRI images. Meanwhile, Roth et al. \cite{past16} presented a method to detect organ at certain body part. They trained their deep neural network with 4298 images and achieved an error rate of 5.9%.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-typical-segmentation}
	\caption{Typical Segmentation}
\end{figure}

Segmentation is the process of dividing a digital image into many sub- images \cite{past17}. A segment is a set of pixels, and therefore is also called hyper pixel. The aim of image segmentation is to simplify or alter the representation of the image so that it becomes more easy to understand or analyze. Segmentation is usually used to locate objects or edges in the image. More precisely, segmentation is a process to label each pixel in the image, which makes pixels with the same label have a similar visual feature, such as color, brightness or texture. Moeskops et al. \cite{past18} designed a multiple-scale CNN for accurate tissue segmentation, using multiple patch sizes and multiple convolution kernel sizes to gain multiple scale information of each pixel, and achieved accuracy from 82\% to 91\%. Zhang et al. \cite{past19} tested four CNN on the task of brain tissue segmentation. Their experiment uses three convolution layers and a fully connected layer, and proved CNN significantly better than traditional methods.

Labeling and captioning is the most widely used way to describe contents in an image. It is the classic classification problem in the area of medical images. Continuous effort is being put in to ensure disease-specific auto labeling. Inspired by neural networks for regular images, some research \cite{past20} \cite{past21} introduced RNN together with latest advance in computer vision to caption chest radiographs in certain contexts. The authors used image captions in public available dataset to train the CNN. To avoid large error, many normalization techniques were applied. Then the network was used to describe the situation of detected disease.

Computer aided detection or diagnosis involves finding or locating abnormalities and suspicious area, and then alert clinicians. The main aim of computer aided detection is to increase the detection rate of infected area and to decrease false negative due to observer’s mistake. Though it is considered a mature area in medical images, deep learning further improved performance in many applications and enabled some design that was impossible in the past. Traditionally, computer detection requires a preprocessed candidate region and manpower to extract features such as shape or statistics in the region; inly after then the features can be feed into the classifier. However, the advantage of feature learning is the core of the new developments. Deep learning can learn the hierarchical features from the dataset independently instead of depending handcrafted features specially targeted for certain area of knowledge. It soon proved to be the most advanced technology. Ciompi et al. \cite{past22} trained CNN with predefined OverFeat as feature extractor, and showed that CNN is feasible to provide useful feature description in lung images. Gao et al. \cite{past23} trained the model from the very beginning. They solved the overfitting problem by randomly cropping or jittering the original image, and then feed the sub images into the model. Finally, the model was able to classify patches into normal, fibrosis and other four abnormal classes.

Due to the prosperity in research, more and more commercial attempts is being conducted recently. Startups entering the medical AI area is increasing. From 2012 to 2016, investments in medical AI increases from 20 cases per year to 70 cases per year. More than 100 large companies are trying to apply deep learning in order to decrease time to provide aids to patient and to automatically diagnosis disease with medical images. IBM Watson Group is supporting a research to screen cancer patients with an affordable procedure. They are trying to make deep learning suitable for production. Other startups include SkinVision, Flatiron Health and Entopsis \cite{past24}.

\subsection{Objective}

Deep learning has a natural advantage in features learning, which means that it has a potential to be applied to this problem mentioned above. Therefore, we will try to implement a complete automated breast cancer diagnosis system. In this system, we will train a deep learning program which can give advice to pathologists, even if s/he do not know anything about AI.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-out-system}
	\caption{Our Diagnosis System}
\end{figure}

This project involves image classification, object detection and image caption together. It is designed to be able to perform mammogram analysis or pathology analysis, and detect possible tumor location. A deliverable diagnosis and tumor positioning report will be generated at the end which can help them make a more accurate decision on diagnosis. The whole system will have the following functionalities:

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/intro-workflow-of-system}
	\caption{Workflow of Our Diagnosis System}
\end{figure}

\begin{enumerate}
	\item Perform mammogram analysis first
	
	To determine if a tumor is benign or malignant, we will first require the patient's magnification mammogram image. The deep learning program will try to make a preliminary classification: cancer, not cancer, or not sure. More detailed diagnosis should follow.
	
	\item Detect possible tumor location if classified positive
	If the program categorizes image as positive, it will further detect the exact existence of tumor. It will point out the most suspicious regions in the image for pathologists' reference.
	
	\item Make a more confident judgment with pathology analysis
	If the program cannot achieve a pre-defined certainty threshold, it will suggest a pathology analysis. As the pathology analysis can give more information, very likely the program will approach the correct inference.
	
	\item Generate human-readable report
	At the last, the program will describe its output in an understandable way. The report will indicate all its findings.
\end{enumerate}

In term one, our primary objective was to build an accurate breast cancer histopathological image classification model, the first computer diagnosis procedure in the workflow. This is the entry point, and will be the most frequently used module in the system.

In term two, our primary objective is to finish up the rest parts of the project, i.e. the tumor detector and the report generator. The program will try to locate tumor cells and tell the pathologist its suggestions. Since it will give more accurate information about the sample, the accuracy may be lower in turn. For users' reference, we plan to explicitly show confidence level on the output reports.
