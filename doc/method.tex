\section{Method}\label{method}

\subsection{Histopathological Image Classification}

This is what we have done in term one. We used the BreakHis dataset to train our ResNet convolutional neural network. The following sections will introduce the design of this model.

\subsubsection{Dataset}

For our project, we are using the Breast Cancer Histopathological Image Classification (BreakHis) dataset. It is composed of 9,109 breast tumor tissue microscopic images. The researchers collected samples from 82 patients, and used different magnifying factors (40x, 100x, 200x, and 400x) to process them [40]. The statistics of samples are illustrated in table \ref{method:breakhis:dataset}

\begin{table}[htp]
	\centering
	\begin{tabular}{ccccc}
		\hline
		Class     & 40x  & 100x & 200x & 400x \\ \hline
		Benign    & 625  & 644  & 623  & 588  \\ 
		Malignant & 1370 & 1437 & 1390 & 1232 \\ 
		Total     & 1995 & 2081 & 2013 & 1820 \\ \hline
	\end{tabular}
    \caption{Distribution of Images}
    \label{method:breakhis:dataset}
\end{table}

The samples are stained with hematoxylin and eosin. The author of the dataset uses breast tissue biopsy slides to generate these samples. Pathologists from the P\&D lab labeled them. The breast tumor specimens were asses by Immune histochemistry. The biopsy procedure was Surgical Open Biopsy.

An Olympus BX-50 system microscope was used to capture the images. As aforementioned, they captured image under four magnification factor, 40x, 10x 200x and 400x. The raw image was stored into the dataset without any normalization of color standardization to avoid loss of information and complexity in analysis. The images were in Portable Network Graphics (PNG) format, in 3-channel RGB, 8-bit depth. A sample was demonstrated in figure \ref{method:breakhis:sample}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-breakhis-sample}
	\caption{Same Tumor under Different Magnification}
	\label{method:breakhis:sample}
\end{figure}

\subsubsection{Preprocess}

In this section, we will discuss how we manipulate the image before feeding it into the model. We proposed different methods, and would compare them in experiments.

\paragraph{Data Augmentation}

Since we are training a deep learning neural network, the amount of train data is a critical problem. The size of the original dataset, 9109, is relatively small for our model, and is therefore very likely to cause overfitting. Summarizing the methods used in past works [41], we can propose multiple ways to extend the dataset systematically.

We do not propose any color standardization since all images have the same color pattern, i.e. pink or purple. This is due to the stain method applied to tissue samples. The data augmentation methods we propose include only geometric transformation. They include:

\begin{enumerate}
	\item rotations: random with angle
	\item translations: random with shift
	\item flipping: true or false
	\item shearing: random with angle
	\item stretching: random with stretch factor between 1/1.3 and 1.3 
\end{enumerate}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-breakhis-augmentation}
	\caption{Examples of Data Augmentation}
\end{figure}

\paragraph{Sliding Window Crop}

It is hard to process the high-resolution images since applying deep learning algorithms on larger image sizes will tend to make the model architecture more complicated. The model will usually have more layers, more parameters which increase the complexity dramatically. Training and tuning the model may be very costly in such case.

One way to solve this problem is sliding window crop. Set a window of size n×n, slide through the image at $step=0.5n$, and then crop [42].

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-breakhis-sliding-window-crop}
	\caption{Examples of Sliding Window Crop}
\end{figure}

Overlaps between crops are deliberately designed to avoid damaging the structure information too much. The number of total crops is given by the following formula:
$$ \#(crop) = 2 \times \frac{IMGWIDTH}{n} \times 2 \times \frac{IMGHEIGHT}{n} $$

\paragraph{Random Crop}

Another way to solve the aforementioned oversized problem is random crop. Set a window of size n×n, do random crop instead of sliding. This is similar to the previous method.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-breakhis-random-crop}
	\caption{Examples of Random Crop}
\end{figure}

The number of total crops is not fixed. However, a higher number of crops will give more information. There will be no limit on how the random selector crop: it may or may not capture the most important features.

For benign samples, there will be no problem. However, for malignant samples, we cannot make sure tumor exist in every crop. Crops extracted from malignant images may actually contains no tumor and should be classifies as benign. This introduces noise in train data.

The gain, on the other hand, is we keep the size of network small. This benefits in various ways: less computation complexity, less logic complication, and most importantly, it reduces chance of overfitting by limiting the parameters of the model to a reasonable amount.

\paragraph{Resizing}

There always exists the method of simply shrinking the image. To avoid moiré after resizing, we will resample the image using pixel area relation. This is the best image interpolation method for decimation since it tends to give a clearer image. This makes the high-resolution image generation pointless, however. 

\paragraph{Whitening}

Whitening is the one of the standard preprocess methods for machine learning. The main idea is to remove extra information dimensions in the image. First, we represent the input dataset as
$$ \{ x_1, \ldots, x_m \} $$
Then we computes the covariance matrix of $x$
$$ \Sigma = \frac{1}{m} \sum_{i=1}^m{x_ix_i^T} $$
Therefore, we can have
$$ x_{rot}=U^T x $$
where $U$ is the eigenvector of $\Sigma$.

This process maps $x$ into a new space that eliminates the correlation between features. Then we can have
$$ x_{PCAwhite}=\frac{x_{rot}}{\sqrt{\lambda_i}} $$
which normalizes the dataset \cite{past43}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-breakhis-whitening}
	\caption{Before and After Whitening}
\end{figure}

After whitening, the new image satisfies two properties: features are less correlated, and features have the same variance. This will significantly accelerate the training process.

\paragraph{Contrast Limited AHE}\label{CLAHE}

Contrast-Limited Adaptive Histogram Equalization (CLAHE) can improve local contrast without damaging the image too much. Consider an image whose pixel values are limited to a specific range, it would be better to have the values distributed in all regions of the channel. This will usually improve the contrast of the image. Therefore, we need further scatter pixels clustered in the “brighter” regions.

Adaptive Histogram Equalization (AHE) will do this work. However, it sometimes will cause loss of information due to over exposing some region that is already bright. This is because the image is not perfectly limited in a small region of the channel. To solve this problem, we can use CLAHE \cite{past44}. The image is divided into tiles, and each tile can perform AHE on its own. For a tile, the brightness across this small area is more likely to be confined. In this way, the image will be clearer.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-breakhis-clahe}
	\caption{Before and After CLAHE}
\end{figure}

Generally speaking, CLAHE is more important than whitening for deep neural networks since the network can learn how to whiten images itself without manually specify it should do it.

\subsubsection{Model Construction} \label{method:breakhis:model-construction}

In this section, a detailed description of our normal model architecture will be given. To understand the construction easier, an visualization version is 
shown in Figure~\ref{fig:71}, left, the format of the text in the box represents kernel size and output channel number of the box's corresponding layer.

Firstly, we considered the input tensor $\bm{I}$ with size $(N,H,W,3)$, where $N,H,W$ are
number of batch, image height, image width respectively, which are specified as a hyper-parameters and 3 indicates that the image has 3-channel(RGB).
When we feed the input tensor into our model, two conventional convolution layers and one maximum pooling will firstly be passed through. 
The output tensor size will become $(N,H/2,W/2,16)$.

The output tensor will be transformed into Residual Network, one box of the Residual Network part
in Figure~\ref{fig:71} is actually one residual block which is introduced in section~\ref{Residual layer}.
When the dimensions increase, the layer is performed with a stride of 2 so that the feature maps size is half.
Therefore, the tensor size will undergo a transformation: $(N,H/2,W/2,16)\rightarrow(N,H/4,W/4,32)\rightarrow(N,H/8,W/8,64)\rightarrow(N,H/16,W/16,128)\rightarrow(N,H/32,W/32,256)$

After passing through all residual blocks, a average pooling layer of
will be inserted, making the tensor size changed from $(N,H/32,W/32,256)$ to $(N,1,1,256)$
Then, a fully-connected layer which indicates the prediction labels (binary classification tasks: malignant or benign) will be deployed
so that the final tensor shape is $(N,1,1,2)$.

\subsubsection{Model Evaluation}

In the previous part, we introduced kinds of methods to do image preprocess, image segmentation and model construction. In this part, we will compare different methods and parameters together and compete with the past paper result using the same dataset to see whether our model is optimized enough.  

Following the experimental protocol proposed in~\cite{past40}, we used cross-validation method~\cite{past49} to do evaluation, the dataset was split so that patients used to build the training set (75\% patients) are not used for the testing set (25\% patients) to guarantee that our model can generalize to those patients not in the dataset, the results presented in this work are the average of four trials with the selected results after converging and a suitable early stop.

Training protocol used here is the purely supervised type, the Stochastic Gradient Descent (SGD) method~\cite{past50}, with backpropagation to compute gradients was used to update the network’s parameters. All fixed hyper-parameters of training are given in the Implementation section. 

The ResNet model were trained on a NVIDIA Tesla K40m GPU~\cite{past53} using the Tensorflow framework. Training took about 5 hours for the 256×256 input size and 10 hours for the 512×512, which is corresponding to a much more complex training set.

When we discuss the results of medical images, there are three ways to report the results in our report: batch level, image level and patient level.

Batch level can be understood by batch-wise, the unit is simply each input we fit into the neuron network. The recognition error at the image level can be calculated by:
$$ \mbox{Image Recognition Accuracy}=\frac{N_{\mbox{correct}}}{N_{\mbox{all}}} $$
Where $N_{\mbox{correct}}$ is the number of cancer images which is correctly classified, and $N_{\mbox{all}}$ is the number of cancer images in the test dataset. 

Patient level is a little different, each patient score is defined as:
$$ \mbox{Patient Score}=\frac{N_{\mbox{correct-in-p}}}{N_{\mbox{p}}} $$
Where $N_{\mbox{correct-in-p}}$ is the number of cancer images of Patient $P$ which is correctly classified,  $N_p$ is the number of cancer images of Patient $P$. Then the global patient error is calculated by:
$$ \mbox{Patient Error}=1-\frac{\sum \mbox{Patient Score}}{\mbox{Total Number of Patients}} $$

Besides basic error results, we also calculated confusion matrix, precision, recall and F1 score~\cite{past54} on either/both image level or/and patient level. Precision, recall and F1 score are defined as:
$$ \mbox{Precision}=\frac{\mbox{True Positive}}{\mbox{True Positive}+\mbox{False Negative}}$$
$$ \mbox{Recall} =\frac{\mbox{True Positive}}{\mbox{True Negative}+\mbox{False Negative}}$$
$$ \mbox{F1 score} =\frac{2 \times \mbox{Precision} \times \mbox{Recall}}{\mbox{Precision} + \mbox{Recall}} $$

Also, we use Area under the curve (AUC) [55] to measure the performance of different models, the AUC of a classifier is equal to the probability that the classifier will rank a randomly chosen positive example higher than a randomly chosen negative example, i.e.
$$ AUC= P(score(x+)>score(x-))$$

Because there are millions of parameters and hundreds of hyper-parameters, therefore which parameters need to be tuned among the test should be considered carefully. Through our study on both medical and deep learning field, we selected three major hyper-parameters (directions) to do our test: Preprocess method, model architecture and image segmentation method. 

For each hyper-parameter, we tested kinds of values or situations based on our guess and motivation, so each block below will contain several sub-blocks to explain each guess and its corresponding results in detail.

\pagebreak
\subsection{Mammogram Tumor Detection}\label{method:detection}

We continue to develop the rest parts of our project in term two. We used the Digital Database for Screening Mammography(DDSM) to train our Mask-RCNN convolutional neural network. The following sections will introduce the design of this model.

\subsubsection{Dataset}

For our project, we are using the Digital Database for Screening Mammography (DDSM) \cite{heath2000digital} as our train dataset. It is specially designed for usage by image analysis researchers. It was a collaborative project by the Massachusetts General Hospital, the University of South Florida, and Sandia National Laboratories. There are approximately 2500 cases in this database, each of which includes a MLO view and a CC view of two breasts, together with some meta information. The statistics of samples are illustrated in table~\ref{method:ddsm:dataset}

\begin{table}[htp]
	\centering
	\begin{tabular}{c c}
		\hline
		Class     & Count \\ \hline
		Benign    & 870   \\
		Malignant & 914   \\
		Normal    & 695   \\
		Total     & 2479  \\ \hline
	\end{tabular}
	\caption{Distribution of Images}
	\label{method:ddsm:dataset}
\end{table}

The meta information associated with a image includes age at time of study, keyword description of abnormalities, scanner resolution, etc. Images with suspicious area are linked with an extra file representing the boundary and types of the region. This provides data for training a more sophisticated object detection algorithm.

This dataset consists of images from different kind of scanners including DBA, HOWTEK and LUMYSIS. The resolution varies from 42 microns to 50 microns. This adds extra complexity to data normalization. The properties of the images are slightly different from each other. Fortunately, the content of images are of the same pattern and is capable for analysis. A example is show below, where it can been seen that each image carries a different size.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-ddsm-sample}
	\caption{Sample from DDSM dataset}
\end{figure}

\subsubsection{Preprocess}

Unfortunately, the DDSM dataset was made 20 years ago. It is using a data format that modern softwares do not recognize. The preprocessing step requires extra care.

\paragraph{LJPEG}

The dataset compresses images with Lossless JPEG(LJPEG) format which is developed by Stanford University. It perform best for gray scale images such as mammogram slides. Since the scanner used to generate these images were set a resolution between 42 to 100 microns, the files are very large. After decompressing, the expected output file usually has a four times larger size. However, this format not a widely supported.

\paragraph{Chain Code}

Boundaries of suspicious regions are represented by chain codes. The chain code uses one digit to specify the relative position of the next pixel in the boundary. Given a starting point, simply move the cursor up, down, left or right to draw a circle on the image, and the circle will be the boundary. The numbers to the direction correlation can be illustrated as in figure \ref{method:ddsm:chain-code}

\begin{table}[htp]
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		& \multicolumn{3}{c|}{X$\rightarrow$} \\ \hline
		\multirow{3}{*}{\begin{tabular}[c]{@{}c@{}} Y\\$\downarrow$ \end{tabular}} & 7           & 0           & 1          \\ \cline{2-4} 
		& 6           & $\cdot$           & 2          \\ \cline{2-4} 
		& 5           & 4           & 3          \\ \hline
	\end{tabular}
	\caption{Chain Code Direction}
	\label{method:ddsm:chain-code}
\end{table}

\paragraph{Contrast Limited AHE}

Similar to term 1, we also use Contrast-Limited Adaptive Histogram Equalization (CLAHE) to improve the quality of images. The detail of the algorithm is illustrated in section \ref{CLAHE}.
\paragraph{Image Augmentation}
Commonly used image augmentation methods are adopted in our project, including left-flip and right-flip. Rotation is not used considering the property of mammogram: all mammograms we used to train and test
are left-view or right-view, we do not hope to break this property. But rotation is still a good point to have a try.

\subsubsection{Model Construction}

Similar with section \ref{method:breakhis:model-construction}, this section is a detailed description on how our model works.
The input tensor, firstly, is a preprocessed image including resizing, padding and cropping and the shape of the input tensor is $(2048,2048,3)$ to maximize the use of image information.

Firstly, the input tensor is fed into a \textbf{base model}, ResNet101 in our case, and the returned tensor are actually the feature maps of image, then the feature maps wil used as input in
other models including RPN, Classification and Mask Generation. All of the functionality of these models are introduced in Section~\ref{tech:rcnn}.

\paragraph{RPN}RPN (Section~\ref{tech:rcnn:rpn}) is the first network using feature maps as input because all other networks need proposals as input, which is
the output of RPN. RPN generates a set of ROIs (still in the format of tensor) with shape $(N_{ROI},4)$, where $N_{ROI}$ represents the number of predicted region proposals.
Typically, the RPN outputs some "valid" anchors calculated from: 
\begin{itemize}
	\item  \textbf{anchor location ($A_{l}= (x,y)$)} indicates the middle point location of anchor.
	\item  \textbf{anchor scale($A_s $)} indicates the scale of the anchor, there are 5 options eg.($32\times32,64\times64,128\times128,256\times256,512\times512$).
	\item \textbf{anchor ratio($A_r = (r_x,r_y)$)} indicates the length:width ratio of the anchor, there are 3 options eg.($1:1,1:2,2:1$)
	\item \textbf{anchor class($A_c$)} indicates whether the anchor is background or not.
	\item \textbf{anchor regression($A_{reg} = (reg_x,reg_y,reg_w,reg_h)$)} indicates the regression value calculated, where $reg_x,reg_y$ indicate the shift value of anchor location $A_l$ and $reg_w,reg_h$ indicate the shift value of anchor scale.
\end{itemize}
Finally, anchors whose class are not background after regression are the final predicted proposals
and are regarded as input of following networks.
\paragraph{ROIAlign} ROIAlign is the middle network to process the proposals generated by RPN and the effect of ROIAlign is for
getting the corresponding areas in feature maps (from backbone model) of proposals (from RPN), the method details are explained in Section~\ref{tech:rcnn:ROIALIGN}.
Colloquially speaking, ROIAlign provides a bridge connecting RPN which outputs proposals in \emph{image} and following fully connected layers which require \emph{feature maps} of images.
\paragraph{Fully Connected Layers and Mask Generation Layer} both fully connected layers and mask generation layer require feature maps generated by ROIAlign as input.
Fully connected layer outputs two informations, proposal's classification and regression strategy. The mask generation layer is actually a CNN whose output is class-specific binary masks of same resolution with proposals.
The relationship between ROIAlign and following layers is illustrated in Figure~\ref{fig:maskrcnn}. Therefore, the final results will contain two elements: the one is the class label for the mask and another one is 
its corresponding masks in raw image.
\begin{figure}[htp]
	\centering
	\includegraphics[width=13cm]{fig/maskrcnn}
	\caption{The overview architecture of fully connected layers and mask generation layer and the relationship with ROIAlign}
	\label{fig:maskrcnn}
\end{figure}
\subsubsection{Loss Function}\label{method:loss}
This work is a multi-stage work which contains multiple sub-stacks including RPN,ROIAlign and so on. Therefore the loss function should be treated
carefully because accumulated loss has a critical influence on this kind of work. Typically, \textbf{we proposed a new loss function for bounding box regression}, which has
an good influence on the result. The loss of different part will also be covered one by one.

RPN loss includes two parts, classification loss (whether the predicted box is positive or not) and regression loss.
The loss function for an anchor is defined as:
\begin{equation}
\label{eq:rpnloss}
{L(p,t)= L_{class}(p,p') + \Lambda p'L_{reg}(t,t').}
\end{equation}
Here, $p$ is the predicted probability of this anchor being an object.
$p'$ is the ground truth label and 1 if the anchor is positive and 0 if negative.
$t$ is a vector representing the 4 parameterized coordinates
of the predicted bounding box, and $t'$ is the corresponding parameterized coordinates
of one positive anchor. The classification loss $L_{class}$ is a log loss over two classes.
For regression loss $L_{reg}$, smooth $L_1$ is used so that $L_{reg}(t,t')=R(t-t')$ where R is the
robust loss function.

In~\cite{maskrcnn}, the parametrizations of t is adopted by:
\begin{equation}
\label{eq:tx}
{t_x = (x-x')/w',t_y =(y-y')/h',t_w=log(w/w'),t_h=log(h,h')}
\end{equation}
where x,y,w,h denote the box's center coordinates and its width and height.
Variables $x'$ is for the ground truth box respectively (likewise for y,w,h).
Our new parametrizations is similar with former one:
\[
	t_x = 
	\begin{cases}
		0, & \text{if} int(x'_0>x_0) + int(x'_3>x_3) = 1   \\ 
		\frac{max(x'_0-x_0,x'_3-x_3)}{w}, & \text{otherwise}
	\end{cases}
\]
where $x_0,x_3$ is the first and last x value from left-up to right-bottom.
The loss function is changed due to fit with the postive dicision method(Overlapping Ratio).

\subsubsection{Model Evaluation}\label{detection:evaluation}

This task is actually a object detection task, a commonly used
evaluation method specified in \cite{everingham2010pascal} is used.

\paragraph{Overlapping Ratios and Intersection of Union}\label{method:iou}
Firstly, the concept of \emph{Overlapping Ratios}(\textbf{OR}) is proposed.
It should be noticed that we \textbf{do not} adopt a commonly index , \emph{Intersection of Union(IOU)}
to define whether one proposal is positive or negative when do evaluation. IOU is only used
to define positive or negative samples when training.
IOU takes the set A of proposed object pixels and the set of true object pixels B and calculates:
\begin{equation}
\label{eq:iou}
{IoU(A, B) = \frac{A \cap B}{A \cup B}}
\end{equation}
Commonly, $IoU > 0.5$ means that it was a hit, otherwise it was a fail. 
Also, considering the property of our cases, which are medical rumor detection.
Therefore,$A\cap B$ is much important, another evaluation value is proposed by us:
\emph{Overlapping Ratios}(\textbf{OR}),which is calculated by:
\begin{equation}
\label{eq:riou}
{OR(A, B) = \frac{A \cap B}{A}}
\end{equation}
Usually, higher IOU and OR stands for a more accurate prediction of proposal.
\paragraph{Mean Average Precision and False Positive Per Image}
For each class, one can calculate the
\begin{itemize}
	\item \textbf{True Positive} TP(c): a proposal was made for class c and there actually was an object of class c
	\item \textbf{False Positive} FP(c): a proposal was made for class c, but there is no object of class c.
	\item \textbf{Average Precision} for class c: $\frac{TP(c)}{TP(c) + FP(c)}$
	\item \textbf{False Positive Per Image(FPPI)} is the average number of false positive samples per image.
\end{itemize}
Then, the \textbf{mAP} (mean average precision),which is used to do the final evaluation,is calculated by:
\begin{equation}
\label{eq:map}
{mAP = \frac{1}{|classes|}\sum_{c \in classes} \frac{\#TP(c)}{\#TP(c) + \#FP(c)}}
\end{equation}
Usually, higher mAP and lower FPPI indicates better performance of detection model.
\paragraph{Mean Sensitive}
$Sensitive$ is defined as:
\begin{equation}
\label{eq:sensitive}
{Sensitive = \frac{Number\ of \ succefully \ predicted\ truth\ box}{Number\ of\ truth\ box}}
\end{equation}
$Mean Sensitive$ is the average sensitive among all images' sensitive.
According to the definition, it is clearly that mean sensitive, especially in medical field, is highly important:
when the value of mean sensitive is approximately 1.0, it means that almost all breast mass are found by the model,
which is pretty valuable. 
%TODO:\subsubsection{Loss Calculation}
\pagebreak
\subsection{User Interface}

At last, we designed a user interface for doctors. It has a web portal, and can generate human-readable reports for users with little prior knowledge about the inner design of the system. The following sections will discuss the design of the user interface.

\subsubsection{Report Generator}

The report generator will get diagnosis result from our model, and then make a human readable report. It should display the confidence level on the report for users to further decide if more check should be performed. The report will consist of an image showing suspected tumor location and a short text suggestion.

\subsubsection{Web Front end}

The web front end will accept image file input from users and display the detection result. The user should be able to select an image from the hard drive and upload the file with ease. The web page will perform basic file validation before really uploading it to server. Since the image tends to be very large, and it takes sometime for server to process an image, a progress bar indicating which step the image is in will be helpful for users. A simple authentication is possible to be added, but may not be required if the server is in local network.

\subsection{Workflow}

To develop a more accurate model, we have the following development workflow cycle in figure \ref{method:workflow}. The cycle includes five elements: design, implement, train, validate and test.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,height=18em,keepaspectratio]{fig/method-workflow}
	\caption{Development Workflow}
	\label{method:workflow}
\end{figure}

After we implement a model, we will train it. Validation will be performed occasionally. If the validation result is not satisfying, we will cut off the training and attempt to find out the reason. After the train accuracy converges, we will do a thorough test of the model and compute some quantitative measurement to determine if our design works well. We will try to analyze the factors that affect the performance, and perform incremental modifications accordingly.
