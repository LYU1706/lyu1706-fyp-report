import numpy as np
import matplotlib.pyplot as plt
import re
MPR= []
MSEN=[]
MACU =[]
MFPP = []
xtick = []
a = []
'''
f =  open('PRO.txt','r')
for line in f:
    tmp =re.findall(r"[-+]?\d*\.\d+|\d+", line)
    a = a+tmp
for i in range(len(a)):
    if i%6 == 2:
        MPR.append(a[i])
    elif i%6 == 3:
        MSEN.append(a[i])
    elif i%6 == 4:
        MFPP.append(a[i])
    elif i%6 == 5:
        MACU.append(a[i])
print(MPR)
print(MSEN)
print(MFPP)
print(MACU)
'''
#0.5545023696682464, 0.5733333333333334, 0.4411764705882353, 0.4182027649769585, 0.3371150729335494, 
#0.6814814814814815, 0.6888888888888889, 0.5074626865671642, 0.5522388059701493, 0.40298507462686567,
#2.785185185185185, 3.0814814814814815, 2.6940298507462686, 3.7686567164179103, 3.0522388059701493,
MPR = [0.5009041591320073, 0.5296, 0.3974025974025974, 0.3325942350332594, 0.22905027932960895] 
MSEN = [ 0.5789473684210527, 0.5984848484848485, 0.43548387096774194, 0.4015748031496063, 0.24731182795698925] 
MFPP = [ 2.075187969924812, 2.227272727272727, 1.8709677419354838, 2.37007874015748, 1.4838709677419355] 
#[0.562962962962963, 0.5333333333333333, 0.4626865671641791, 0.44776119402985076, 0.4253731343283582, 0.556390977443609, 0.5303030303030303, 0.45161290322580644, 0.4251968503937008, 0.45161290322580644]
#MPR =MPR[:14]
MPR.reverse()
#MSEN = MSEN[:14]
MSEN.reverse()
#MFPP = MFPP[:14]
MFPP.reverse()
x = np.arange(len(MPR))+1
fig, ax1 = plt.subplots(figsize=(6, 3))
ax1.set_ylabel('mPresion,mSensitive')
ax1.plot(x, MPR,label = "Mean Average Precision")
ax1.plot(x,MSEN,label = "Mean Sensitive")
ax1.axvline(x=1,color="black")
ax1.axvline(x=2,color="black")
ax1.axvline(x=4,color="black")
ax1.axvline(x=5,color="black")
ax1.set_xlabel('epoch')
'''
ax1.axvline(x=16,color="black")
ax1.axvline(x=22,color="black")
ax1.axvline(x=28,color="black")
'''
ax1.legend()
ax2 = ax1.twinx()
ax2.set_ylabel('FPPI')
ax2.plot(x,MFPP,label="FPPI",color="r")
ax2.legend(loc=4)
plt.show()
